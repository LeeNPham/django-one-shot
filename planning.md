Isolate a single fail in the terminal

Look at the exact assertion difference.

look at the test itself

it is referencing the pk and that's the value that's different
create does not redirect to detail
original detail page *****, 
has to do with create todo item redirect, not with todo list redirect

redirect error?
    redirects occur in views.py
        to look at views 
            is supposed to redirect to todo list detail
                view displays todolist detail so I so far see no errors?

i only have 2 pages i have the redirect error on possibly

        the options are the todos/items/1/edit

            and the todos/1 
            which has to do with the detail page of the todolist

because my error is todos/1/vstodos/2/, theres a higher chance my issue is with how its rendered in the 

todolist detail view with todolistdetail.html


If the to-do item is successfully edited, it should redirect to the detail page for the to-do list.

FAIL: test_create_redirects_to_detail (tests.test_feature_13.TestTodoItemUpdateView)
----------------------------------------------------------------------
Traceback (most recent call last):
  File "C:\Users\Blade\Desktop\Operations\HackReactor\sjp-july\projects\django-one-shot\tests\test_feature_13.py", line 155, in test_create_redirects_to_detail
    self.assertEqual(
AssertionError: '/todos/2/' != '/todos/1/'
- /todos/2/
?        ^
+ /todos/1/
?        ^
 : Create does not redirect to detail
















This feature allows a person to go from the to-do list page to a page that allows them to create a new to-do item.

Create a create view for the TodoItem model that will show the task field in the form and handle the form submission to create a new TodoItem.

If the to-do list is successfully created, it should redirect to the detail page for that to-do list.

Register that view for the path "items/create/" in the todos urls.py and the name "todo_item_create".

Create an HTML template that shows the form to create a new TodoItem (see the template specifications below).

Add a link to the list view for the TodoList that navigates to the new create view.
Template specifications