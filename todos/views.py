from django.shortcuts import render, redirect, get_object_or_404
from .models import TodoItem, TodoList
from .forms import TodoitemForm, TodolistForm


def todo_list_create(request):
    if request.method == "POST" and TodolistForm:
        form = TodolistForm(request.POST)
        if form.is_valid():
            todolist = form.save()
            return redirect("todo_list_detail", pk=todolist.pk)
    elif TodolistForm:
        form = TodolistForm()
    else:
        form = None
    context = {
        "form": form,
    }
    return render(request, "todos/todo_list_new.html", context)


def todo_list_update(request, pk):
    if TodoList and TodolistForm:
        instance = TodoList.objects.get(pk=pk)
        if request.method == "POST":
            form = TodolistForm(request.POST, instance=instance)
            if form.is_valid():
                form.save()
                return redirect("todo_list_detail", pk=pk)
        else:
            form = TodolistForm(instance=instance)
    else:
        form = None
    context = {
        "form": form,
    }
    return render(request, "todos/todo_list_update.html", context)


def todo_list_list(request):
    context = {
        "todolist": TodoList.objects.all,
    }
    return render(request, "todos/todo_list_list.html", context)


def todo_list_detail(request, pk):
    context = {"todolist_detail": TodoList.objects.get(pk=pk)}
    return render(request, "todos/todo_list_detail.html", context)


def todo_list_delete(request, pk):
    context = {}
    todolist = get_object_or_404(TodoList, pk=pk)
    if request.method == "POST":
        todolist.delete()
        return redirect("todo_list_list")

    return render(request, "todos/todo_list_delete.html", context)


def todo_item_create(request):
    if request.method == "POST" and TodoitemForm:
        form = TodoitemForm(request.POST)
        if form.is_valid():
            todoitem = form.save()
            return redirect("todo_list_detail", pk=todoitem.list.id)
    elif TodoitemForm:
        form = TodoitemForm()
    else:
        form = None
    context = {
        "form": form,
    }
    return render(request, "todos/todo_item_create.html", context)


def todo_item_update(request, pk):
    if TodoItem and TodoitemForm:
        instance = TodoItem.objects.get(pk=pk)
        if request.method == "POST":
            form = TodoitemForm(request.POST, instance=instance)
            if form.is_valid():
                form.save()
                return redirect("todo_list_detail", pk=instance.pk)
        else:
            form = TodoitemForm(instance=instance)
    else:
        form = None
    context = {
        "form": form,
    }
    return render(request, "todos/todo_item_update.html", context)
